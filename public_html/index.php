<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />

    <title>Webpack starter</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <link href="dist/main.css" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-sm-12 col-lg-3 logo-button">
                <div class="logo">
                    <div class="logo__img">
                        <img src="dist/img/logo.png">
                    </div>
                    <div class="logo__text">EDUCATION</div>
                </div>
                <button class="hamburger hamburger--vortex" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
            </div>
            <div class="col-lg-9">
                <div class="menu__header">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Courses</a></li>
                        <li><a href="#">Teachers</a></li>
                        <li><a href="#">Programs</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="slider">
    <div class="slider__item" style="background-image: url('dist/img/banner_photo.png')">
        <div class="slider__block">
            <div class="slider__row">
                <p class="slider__title">Knowledge is power</p>
                <p class="slider__text">Any succesfull career starts with good education</p>
                <a href="#" class="slider__button">Learn more</a>
            </div>
        </div>
    </div>
    <div class="slider__item" style="background-image: url('dist/img/banner_photo.png')">
        <div class="slider__block">
            <div class="slider__row">
                <p class="slider__title">Knowledge is power</p>
                <p class="slider__text">Any succesfull career starts with good education</p>
                <a href="#" class="slider__button">Learn more</a>
            </div>
        </div>
    </div>
    <div class="slider__item" style="background-image: url('dist/img/banner_photo.png')">
        <div class="slider__block">
            <div class="slider__row">
                <p class="slider__title">Knowledge is power</p>
                <p class="slider__text">Any succesfull career starts with good education</p>
                <a href="#" class="slider__button">Learn more</a>
            </div>
        </div>
    </div>
</div>
<div class="education">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 .col-lg-12 education__img">
                <img src="dist/img/education.png">
            </div>
            <div class="offset-xl-1 col-xl-5 .col-lg-12">
                <div class="education__text">
                    <div class="education__title">
                        Education
                    </div>
                    <div class="education__description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div>
                </div>
                <div class="skills">
                    <div class="container">
                        <div class="row">
                            <div class="col-1 skills__img">
                                <img src="dist/img/learn_anything.png">
                            </div>
                            <div class="col skills__title">
                                Learn Anything
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1 skills__img">
                                <img src="dist/img/talk_to_instructor.png">
                            </div>
                            <div class="col skills__title">
                                Talk To Our Instructors
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1 skills__img">
                                <img src="dist/img/speak_others.png">
                            </div>
                            <div class="col skills__title">
                                Speak With Others
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="features__title">Features</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row d-flex align-content-around">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/online_test.png">
                </div>
                <h3 class="features__h3">Online Testing</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/expert_professor.png">
                </div>
                <h3 class="features__h3">Expert Professors</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/trusted_certification.png">
                </div>
                <h3 class="features__h3">Trusted Certifications</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/online_test.png">
                </div>
                <h3 class="features__h3">Online Testing</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/expert_professor.png">
                </div>
                <h3 class="features__h3">Expert Professors</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 d-flex align-items-center flex-column">
                <div class="features__img">
                    <img src="dist/img/trusted_certification.png">
                </div>
                <h3 class="features__h3">Trusted Certifications</h3>
                <div class="features__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="courses container">
    <div class="row">
        <div class="col courses__title">Courses</div>
    </div>
    <div class="row courses__row">
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 course">
            <div class="course__img" style="background-image: url('dist/img/web_design.png');"></div>
            <div class="course__body">
                <div class="course__title">
                    Web Design
                </div>
                <div class="course__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
                </div>
                <div class="course__hr"></div>
                <div class="course__time">
                    <span>Time : 10 am</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__teacher">
                    <span>Teacher: Ressie Rottman</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__button">
                    <div>Join Now</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 course">
            <div class="course__img" style="background-image: url('dist/img/web_design.png');"></div>
            <div class="course__body">
                <div class="course__title">
                    Photography
                </div>
                <div class="course__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
                </div>
                <div class="course__hr"></div>
                <div class="course__time">
                    <span>Time : 6 pm</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__teacher">
                    <span>Teacher: Floyd Fukuda</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__button">
                    <div>Join Now</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 course">
            <div class="course__img" style="background-image: url('dist/img/web_design.png');"></div>
            <div class="course__body">
                <div class="course__title">
                    Digital Marketing
                </div>
                <div class="course__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
                </div>
                <div class="course__hr"></div>
                <div class="course__time">
                    <span>Time : 10 am</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__teacher">
                    <span>Teacher: Elena Cully</span>
                    <div class="course__hr"></div>
                </div>
                <div class="course__button">
                    <div>Join Now</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row courses__footer">
        <div class="col">View All</div>
    </div>
</div>

<div class="teachers">
    <div class="container">
        <div class="row col teachers__title">
            Our Teachers
        </div>
    </div>
    <div class="teachers__mobile">
        <div class="container">
            <div class="row teachers__slider d-flex">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="teachers__desktop">
        <div class="container">
            <div class="row d-flex">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 teachers__col">
                    <div class="teachers__img">
                        <img src="dist/img/teacher_1.png">
                    </div>
                    <div class="teachers__text">
                        <div class="teachers__name">
                            Velvet Vachon
                        </div>
                        <div class="teachers__prof">
                            Design Head
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="programs">
    <div class="container">
        <div class="row">
            <div class="col programs__head">Programs</div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_1.png');">
                        <div class="programs__title">
                            Pre-college education
                        </div>
                        <div class="programs__text">
                            Broaden the creative
                            horizons of your kids
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_2.png');">
                        <div class="programs__title">
                            Pre-college education
                        </div>
                        <div class="programs__text">
                            Children will deepen the
                            knowledge & skills they obtained
                            in theelementary school
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_3.png');">
                        <div class="programs__title">
                            High school
                        </div>
                        <div class="programs__text">
                            Preparing youngsters for
                            the adult life
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_4.png');">
                        <div class="programs__title">
                            International exchange
                        </div>
                        <div class="programs__text">
                            Accepting students from abroad
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_5.png');">
                        <div class="programs__title">
                            Graduate programs
                        </div>
                        <div class="programs__text">
                            Solidifying knowledge that
                            was obtained through
                            the long years of learning
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="programs__block">
                    <div class="progress__background" style="background-image: url('dist/img/program_6.png');">
                        <div class="programs__title">
                            Home education
                        </div>
                        <div class="programs__text">
                            Providing robust distant education
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reviews">
    <div class="container">
        <div class="row">
            <div class="col reviews__head">What client say</div>
        </div>
    </div>
    <div class="container reviews__container">
        <div class="row">
            <div class="col">
                <div class="reviews__thumbnail">
                    <div>
                        <img src="dist/img/client_1.png">
                    </div>
                    <div>
                        <img src="dist/img/client_2.png">
                    </div>
                    <div>
                        <img src="dist/img/client_3.png">
                    </div>
                    <div>
                        <img src="dist/img/client_4.png">
                    </div>
                    <div>
                        <img src="dist/img/client_5.png">
                    </div>
                    <div>
                        <img src="dist/img/client_2.png">
                    </div>
                    <div>
                        <img src="dist/img/client_5.png">
                    </div>
                </div>
                <div class="reviews__slider">
                    <div>
                        <div class="reviews__text">
                            111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip excas.
                        </div>
                        <div class="reviews__name">
                            John Doe
                        </div>
                        <div class="reviews__prof">
                            CEO Smart Edu
                        </div>
                    </div>
                    <div>
                        222Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                    <div>
                        3333Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                    <div>
                        4444Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                    <div>
                        5555Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                    <div>
                        2222225555Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                    <div>
                        5512312312355Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip excas.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog">
    <div class="container-fluid">
        <div class="row blog__head">
            <div class="col">Blog</div>
        </div>
    </div>
    <div class="blog__slider">
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_5.png')"></div>
                <div class="blog__title">
                    Education with
                    technologies
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_2.png')"></div>
                <div class="blog__title">
                    How to use music in education
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_3.png')"></div>
                <div class="blog__title">
                    Education with
                    technologies
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_4.png')"></div>
                <div class="blog__title">
                    Turning goals into
                    reality
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_5.png')"></div>
                <div class="blog__title">
                    The nation into
                    education
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_3.png')"></div>
                <div class="blog__title">
                    Gamification and
                    learning
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_2.png')"></div>
                <div class="blog__title">
                    Gamification and
                    learning
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_4.png')"></div>
                <div class="blog__title">
                    Gamification and
                    learning
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
        <div>
            <div class="blog__item">
                <div class="blog__img" style="background-image: url('dist/img/blog_2.png')"></div>
                <div class="blog__title">
                    Gamification and
                    learning
                </div>
                <div class="blog__text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu
                    mod tempor incididunt ut labore et
                </div>
            </div>
        </div>
    </div>
    <div class="blog__footer cotainer">
        <div class="row">
            <div class="col">
                <div class="blog__button">Read more</div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid subscribe">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6 subscribe__text">
                        Subscribe to our Newsletter
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="subscribe__form">
                            <div class="subscribe__input">
                                <input type="text">
                            </div>
                            <div class="subscribe__button">
                                <div>
                                    Sign Up
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container contact">
    <div class="row">
        <div class="col contact__head">Contact</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6 order-2 order-sm-2 order-md-1">
            <form action="" class="contact__form">
                <input type="text" name="full_name" placeholder="Full Name">
                <input type="text" name="email" placeholder="E mail">
                <textarea name="message" placeholder="Message"></textarea>
            </form>
            <div class="contact__button">
                <div>Send</div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 order-1 order-sm-1 order-md-2">
            <div class="contact__map">
                <img src="dist/img/map.png">
            </div>
            <div class="contact__info">
                <div>
                    <img src="dist/img/map_1.png">
                    <div class="contact__text">LSE Houghton Street London WC2A 2AE UK.</div>
                </div>
                <div>
                    <img src="dist/img/map_2.png">
                    <div class="contact__text">hello@gmail.com</div>
                </div>
                <div>
                    <img src="dist/img/map_3.png">
                    <div class="contact__text">+44(20) 74057686</div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-3">
                <div class="logo justify-content-start">
                    <div class="logo__img">
                        <img src="dist/img/logo.png">
                    </div>
                    <div class="logo__text">EDUCATION</div>
                </div>
            </div>
            <div class="menu__footer col-lg-9 d-flex justify-content-end">
                <div>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Courses</a></li>
                        <li><a href="#">Teachers</a></li>
                        <li><a href="#">Programs</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="dist/bundle.js"></script>
</html>