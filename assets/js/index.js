import 'slick-carousel';
import 'jquery-match-height';

import './partials/home';

import './partials/slider';

$(".hamburger").click(function() {
    $(this).toggleClass("is-active");
    $('.menu__header ul').toggleClass('menu__header_column');
});

$( window ).resize(function() {
    let width = document.body.clientWidth;
    if (width > 991) {
        $('.menu__header ul').removeClass('menu__header_column');
        $('.hamburger').removeClass("is-active");
    }
});