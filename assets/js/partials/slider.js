$('.slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true
});
$('.reviews__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.reviews__thumbnail',
    adaptiveHeight: true,
    dots: true,
});
$('.reviews__thumbnail').slick({
    slidesToShow: 6,
    slidesToScroll: 4,
    asNavFor: '.reviews__slider',
    focusOnSelect: true,
    centerMode: true,
    centerPadding: 0,
    responsive: [
        {
            breakpoint: 1900,
            settings: {
                slidesToShow: 5,
            }
        },
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 4,
            }
        },
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 1,
            }
        },
    ]
});
$('.blog__slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    centerMode: true,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 1900,
            settings: {
                slidesToShow: 5,
            }
        },
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 4,
            }
        },
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 900,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
});

$('.teachers__mobile .teachers__slider').slick(
    {
        slidesToShow: 1,
        slidesToScroll: 1,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: 0,
        dots: true,
    }
);


const settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    focusOnSelect: true,
    centerMode: true,
    centerPadding: 0,
    dots: true,
    responsive: [
        {
            breakpoint: 2000,
            settings: "unslick"
        },
        {
            breakpoint: 900,
            settings: { slidesToShow: 1}
        },
    ]
};

$( window ).resize(function() {
    $('.teachers__mobile .teachers__slider').slick('refresh');
});

function sliderCourses() {
    const settings = {
        slidesToShow: 1,
        slidesToScroll: 1,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: 0,
        dots: true,
        responsive: [
            {
                breakpoint: 2000,
                settings: "unslick"
            },
            {
                breakpoint: 900,
                settings: {slidesToShow: 1}
            },
        ]
    };

    $('.courses__row').not('.slick-initialized').slick(settings);
}
sliderCourses();

$(window).on('resize', sliderCourses);
