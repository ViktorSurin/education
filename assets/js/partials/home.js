$(window).scroll(function() {
    let header_top = $('.header').offset().top;
    if( header_top > 100 ) {
        $('.header').addClass('header_fixed');
    } else {
        $('.header').removeClass('header_fixed');
    }
});

$(function() {
    $('.course__title').matchHeight();
    $('.course__text').matchHeight();
    $('.course__time').matchHeight();
    $('.course__teacher').matchHeight();
});